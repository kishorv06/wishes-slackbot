FROM python:3.7

WORKDIR /usr/src/wish-bot

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python" , "./wishbot.py" ]