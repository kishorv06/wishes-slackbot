import json
import requests
import urllib.parse

class slack :

    conf = {}

    def slackAPI(self, endpoint, query):
        global conf
        try:
            r = requests.get(conf["url"] + endpoint + "?token=" + conf["token"] + "&" + query)
            return json.loads(r.text)
        except Exception as e:
            print(e)
            exit("Failed to execute the API request.")

    def load_users(self):
        users = []
        all_users = []
        additional_params = ""
        while True:
            resp = self.slackAPI("users.list", additional_params)
            if resp["ok"]:
                all_users += resp["members"]
            else:
                print("Error fetching user list from slack.")
                exit(2)
            
            if "response_metadata" in resp and "next_cursor" in resp["response_metadata"] and resp["response_metadata"]["next_cursor"]:
                additional_params += "cursor=" + urllib.parse.quote_plus(resp["response_metadata"]["next_cursor"])
            else:
                break
        
        for user in all_users:
            if all( filter in user and user[filter] == value for filter, value in conf["user_filters"].items() ):
                users.append(user)
        return users

    def is_matching(self, str1, str2):
        return str1.replace(" ","").lower() == str2.replace(" ","").lower()

    def find_user(self, name, users):
        for user in users:
            if self.is_matching(name, user["profile"]["real_name_normalized"]) or self.is_matching(name, user["profile"]["display_name_normalized"]):
                return user
        return None

    def send_message(self, user, message, debug):
        print("User: {}\nMessage: {}\n\n".format(user,message))
        if not debug:
            self.slackAPI("chat.postMessage","channel={}&text={}".format(user,message))

    def init(self, slack_conf):
        global conf
        conf =  slack_conf