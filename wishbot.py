#!/usr/bin/env python

import os

from datetime import date
from icalevents.icalevents import events
from modules.slack import slack

import json
import re

def parse_calendar(feed_url, regex):
	today = date.today()
	events_today = []
	regex = re.compile(regex)
	for event in events(url=feed_url, start = today, end = today):
		result = regex.match(event.summary)
		if result:
			events_today.append(result.groupdict())
	return events_today

dirname = os.path.dirname( __file__ )

config_file_path = os.path.join( dirname, 'config.json' )
with open( config_file_path ) as config_file:
	config = json.load( config_file )

	slackInstance = slack()
	slackInstance.init(config["slack"])
	all_users =  slackInstance.load_users()

	if config['debug']:
		print("Warning: Debug mode is enabled. Messsages won't be sent.")

	for calendar in config[ 'calendars' ]:
		events_today = parse_calendar(calendar[ "feed_url" ], calendar[ "summary_regex" ])
		channel_message = calendar["messages"]["channel_message"]
		user_format = re.sub(r'.*\[(.*)\].*',r'\1',channel_message) if channel_message != "" else None
		users_list = []
		for event in events_today:
			personal_message = calendar["messages"]["personal_message"]
			channel_item = user_format
			for key, value in event.items():
				if key == "name":
					user = slackInstance.find_user(value, all_users)
					if user:
						value = user["profile"]["first_name"] if "first_name" in user["profile"] else user["profile"]["real_name_normalized"]

				if personal_message != "":
					personal_message = personal_message.replace("{" + key + "}", value)

				if channel_item:
					if key == "name" and calendar["messages"]["mention"] and user:
						channel_item = channel_item.replace("{" + key + "}","<@" + user["id"] + ">")
					else:
						channel_item = channel_item.replace("{" + key + "}",value)

			if user:
				slackInstance.send_message(user["id"],personal_message,config['debug'])

			users_list.append(channel_item)

		if len(users_list) > 0:
			if len(users_list) == 1:
				users = users_list[0]
			else:
				users =  ', '.join(users_list[:-1]) + " and " + users_list[-1]
			slackInstance.send_message(calendar["messages"]["channel"],re.sub(r'\[.*\]',users,channel_message),config['debug'])
