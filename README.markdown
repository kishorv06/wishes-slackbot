# Wishes Slackbot

This bot wishes your team mates on Slack. It can send a message to a Slack channel of your choosing deriving data from an iCal feed. You can even mention the user and send a custom greeting message.

The bot is designed to run as a `cron`job. \
NB : This project requires python3 and above to run

## Inspiration
This project was inspired by [slack-birthday-bot](https://github.com/jeKnowledge/slack-birthday-bot) from which [@shinenelson](https://github.com/shinenelson) ported it into [a Python script](https://gist.github.com/shinenelson/2861380e2ecb5f4e14d8b42ac231cc74). The reasoning behind the port was to reduce the [barriers to entry](https://en.wikipedia.org/wiki/Barriers_to_entry) for deployment. A python runtime is generally available on *NIX-based systems out-of-the-box making it easier to deploy with minimal effort compared to Ruby which would require at least a ruby runtime to be installed on the system the script is being deployed on. From the python script, he got feedback to develop the project to work for anniversaries. After discussing the data structure with [@kishorv06](https://krv.microid.in) to fit for the new requirement, they came up with a better idea with a bigger scope which evolved into wishes slackbot.

## Configuration
1. Create a new [Slack App](https://api.slack.com/start/building).
2. Authorize the new Slack App and obtain a [token](https://api.slack.com/authentication/token-types)
3. Copy the example configuration file `config.example.json` to `config.json` and populate the corresponding configuarations according to your requirement / preference
Note : The filename of the configuration file MUST be `config.json` as it is hard-coded into the bot's code.

```
{
  "calendars" : [                       # JSON array of iCal calendar feeds
    {
      "feed_url" : "",                  # fully-qualified URL of iCal feed
      "summary_regex" : "",             # regular expression of Summary field in iCal
                                        # this will be the field used to extract data
      "messages" : {
        "mention" : <true|false>,       # boolean | mention the user in the channel? or not.
        "channel" : "general",          # channel to send the team-wide greeting to
                                        # leave empty if you don't want to wish the user
                                        # in a public channel
        "channel_message" : "",         # greeting message to be sent to the channel
        "personal_message" : "",        # personal message to be sent to the user
      }
    }
  ],
  "slack" : {                           # slack-specific configuration
    "url" : "https://slack.com/api/",   # fixed URL to invoke slack API
    "token" : "",                       # token obtained in step 2 above
    "user_filters" : {                  # filters for users to iterate upon
                                        # this can be any field that is available
                                        # in the top-most level of the user profile
      "is_bot" : <true|false>,          # boolean
      "deleted" : <true|false>          # boolean
  },
  "debug" : false                       # When debug mode is enabled, messages are not sent
}
```

## Assumptions
There are some general assumptions that are made while coding :
1. The summary regular expression derived from the iCal feed
   has [named group regular expressions](https://docs.python.org/3/library/re.html#index-17)
2. At least one of the named group regular expressions is called `name`
3. `user_filters` uses keys from the top-most level of the user profile

## Mandates
1. The filename of the configuration file MUST be `config.json` as it is hard-coded into the bot's code.
2. All fields in the configuration file are mandatory
3. The `summary_regex` field should have named group regular expressions
   and at least one of them MUST be `name`
4. `channel_message` should have the named groups enclosed within square brackets ( `[]` ).
   Refer to `config.example.json` for more examples.

## Requirements
To install and run the bot, the  project requires a few packages to be installed on the host system :
* git
* build-essential
* python3
* python3-pip
* python3-wheel

## Installation & Hacking

### Docker installation ( recommended )
```
docker run -v /path/to/config.json:/usr/src/wish-bot/config.json kishorv06/wish-bot:latest
```

### Regular installation
A system-wide installation is recommended so that the bot can easily be deployed as a `cron` job.

### system-wide installation
Before running the bot, you will have to install the required dependencies from `requirements.txt`.

```sh
sudo pip3 install -r requirements.txt
```
### local-user installation
```sh
python3 -m venv .$( basename $PWD ).venv
source .$( basename $PWD ).venv/bin/activate
pip3 install wheel
pip3 install -r requirements.txt
```

Once the system is primed with the necessary pacakges required for running the bot,
```sh
python3 wishbot.py
```
should run the bot according to the configurations specified in `config.json`

## Deploy
1. Run `crontab -e` to edit your crontab
2. Add this line to the crontab and save it:
    `@daily cd /clone/location && python Birthday-slackbot.py`
    * replace `/clone/location` with the location where you cloned the repo)
    * the `@daily` `cron` shorthand might vary among servers and/or distributions.
      Replace `@daily` with a time of your preference according to the [`cron` syntax](http://tldp.org/LDP/lame/LAME/linux-admin-made-easy/using-cron.html).
    * Remember to verify the timezone setting of your server or you won't get the expected result.

## Guarantee, Apologies and Exceptions
This bot was developed very quickly in a couple of days. While the functionality of the bot is complete and it works as expected in the ideal scenario, it is not guaratneed to work in all scenarios. For example, exception handling and logging has not been implemented properly. Proper tests could not be developed for the bot due to both lack of time as well as technical reasons ( publicly hosted iCal feed, slack bot API access, test workspace ).

# Things to Do
[x] iCal feed fetching and parsing \
[x] posting to public channel on Slack \
[x] personal message via Slackbot \
[x] slack integration via bot \
[ ] better exception handling \
[ ] better logging \
[ ] unit tests \
[ ] post images of celebrated event with confetti et al. \
